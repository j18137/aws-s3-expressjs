import S3 from "aws-sdk/clients/s3.js";
import { config } from "dotenv";

config();

export const s3Client = new S3({
  region: process.env.AWS_REGION,
  accessKeyId: process.env.AWS_KEY,
  secretAccessKey: process.env.AWS_SECRET,
});

import { Router } from "express";
import multer from "multer";
import {
  uploadFile,
  deleteFile,
  downloadFile,
  getFile,
} from "../controllers/file.js";

const upload = multer();

const fileRouter = Router();

// UPLOAD FILE
fileRouter.post("/create", upload.single("file"), uploadFile);

// DOWNLOAD FILE
fileRouter.get("/download/:bucket/:filename", downloadFile);

// GET FILE
fileRouter.get("/get/:bucket/:filename", getFile);

// DELETE FILE
fileRouter.delete("/delete", deleteFile);

export { fileRouter };

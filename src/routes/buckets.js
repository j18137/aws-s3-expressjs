import { Router } from "express";
import {
  createBucket,
  deleteBucket,
  listBucket,
} from "../controllers/bucket.js";

const bucketRouter = Router();

// UPLOAD BUCKET
bucketRouter.post("/create", createBucket);

// DELETE BUCKET
bucketRouter.delete("/delete", deleteBucket);

// LIST ALL BUCKETS
bucketRouter.get("/list", listBucket);

export { bucketRouter };

import express from "express";
import { fileRouter } from "./routes/files.js";
import { bucketRouter } from "./routes/buckets.js";

const app = express();

// MIDDLEWARES
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// ROUTES

app.use("/file", fileRouter);
app.use("/bucket", bucketRouter);

app.listen(3000, () => console.log("http://localhost:3000"));

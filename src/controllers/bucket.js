import { s3Client } from "../s3/connection.js";

export const createBucket = async (req, res) => {
  try {
    const { bucket } = req.body;

    const bucketParams = {
      Bucket: bucket,
    };
    await s3Client.createBucket(bucketParams).promise();

    res.json({ message: "success" });
  } catch (error) {
    res.status(500).json({ message });
  }
};

export const deleteBucket = async (req, res) => {
  try {
    const { bucket } = req.body;

    const bucketParams = {
      Bucket: bucket,
    };
    await s3Client.deleteBucket(bucketParams).promise();

    res.json({ message: "success" });
  } catch (error) {
    res.status(500).json({ message });
  }
};

export const listBucket = async (req, res) => {
  try {
    const response = await s3Client.listBuckets().promise();

    res.json(response.Buckets);
  } catch (error) {
    res.status(500).json({ message });
  }
};

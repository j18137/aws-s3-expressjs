import { s3Client } from "../s3/connection.js";

export const uploadFile = async (req, res) => {
  try {
    const { buffer, originalname } = req.file;
    const { bucket } = req.body;

    const bucketParams = {
      Bucket: bucket,
      Key: originalname,
      Body: buffer,
    };
    await s3Client.upload(bucketParams).promise();

    res.json({ message: "success" });
  } catch ({ message }) {
    res.status(500).json({ message });
  }
};

export const getFile = async (req, res) => {
  try {
    const { bucket, filename } = req.params;

    const bucketParams = {
      Bucket: bucket,
      Key: filename,
    };
    const response = await s3Client.getObject(bucketParams).promise();

    res.header("Content-Type", "image/png");

    res.send(response.Body);
  } catch ({ message }) {
    res.status(500).json({ message });
  }
};

export const downloadFile = async (req, res) => {
  try {
    const { bucket, filename } = req.params;

    const bucketParams = {
      Bucket: bucket,
      Key: filename,
    };
    const response = await s3Client.getObject(bucketParams).promise();
    res.send(response.Body);
  } catch ({ message }) {
    res.status(500).json({ message });
  }
};

export const deleteFile = async (req, res) => {
  try {
    const { bucket, filename } = req.body;

    const bucketParams = {
      Bucket: bucket,
      Key: filename,
    };
    await s3Client.deleteObject(bucketParams).promise();
    res.json({ message: "success" });
  } catch ({ message }) {
    res.status(500).json({ message });
  }
};
